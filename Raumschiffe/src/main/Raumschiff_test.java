package main;

import java.util.ArrayList;

public class Raumschiff_test {
	
	public static void main(String[] args) {
		
		//Broudcast erstellt
		ArrayList<String> broadcast = new ArrayList<String>();
		
		//Ladung erstellt
		Ladung saft = new Ladung("Ferengi Schneckensaft", 200);
		Ladung schrott = new Ladung("Borg-Schrott", 5);
		Ladung rMaterie = new Ladung("Rote Materie", 2);
		Ladung sonde = new Ladung("Forschungssonde", 35);
		Ladung schwert = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung waffe = new Ladung("Plasma-Waffe", 50);
		Ladung torpedo = new Ladung("Photonentorpedo", 3);
		
		//Schiffe erstellt
		Raumschiff klingonSchiff = new Raumschiff("IKS Hegh'ta", "", 100, 100, 100, 100, 1, 2, broadcast);
		Raumschiff romulanSchiff = new Raumschiff("IRW Khazara", "", 100, 100, 100, 100, 2, 2, broadcast);
		Raumschiff vulkanierSchiff = new Raumschiff("Ni'Var", "", 80, 80, 100, 50, 0, 5, broadcast);
		
		//Ladung der Schiffe gesetzt
		klingonSchiff.addLadung(saft);
		klingonSchiff.addLadung(schwert);
		
		romulanSchiff.addLadung(schrott);
		romulanSchiff.addLadung(rMaterie);
		romulanSchiff.addLadung(waffe);
		
		vulkanierSchiff.addLadung(sonde);
		vulkanierSchiff.addLadung(torpedo);
		
		//Erste Gänge 
		klingonSchiff.photonentorpedoAbschießen(romulanSchiff);
		romulanSchiff.phaserkanoneAbschießen(klingonSchiff);
		vulkanierSchiff.nachrichtSenden("Gewalt ist nicht logisch");
	
		//Klingonsshiff prüft den Zustand und die Ladung 
		klingonSchiff.raumschiffZustandZeigen();
		klingonSchiff.ladungsverzeichnisZeigen();
		
		//Zweite Gänge
		klingonSchiff.photonentorpedoAbschießen(romulanSchiff);
		klingonSchiff.photonentorpedoAbschießen(romulanSchiff);
		
		//Alle Schiffe prüfen den Zustand und die Ladung
		klingonSchiff.raumschiffZustandZeigen();
		klingonSchiff.ladungsverzeichnisZeigen();
		romulanSchiff.raumschiffZustandZeigen();
		romulanSchiff.ladungsverzeichnisZeigen();
		vulkanierSchiff.raumschiffZustandZeigen();
		vulkanierSchiff.ladungsverzeichnisZeigen();
	}
}