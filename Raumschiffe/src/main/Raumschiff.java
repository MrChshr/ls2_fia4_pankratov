package main;

import java.util.ArrayList;


public class Raumschiff {
	//Attributen der Raumschiff
	private String kapitansname;
	private String schiffsname;	
	private int energieversorgungProzent;
	private int schieldeProzent;
	private int lebenserhaltung;
	private int huelleProzent;
	private int photonentorpedoAnzahl;
	private int reparaturandroidAnzahl;
	public ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	//Konstruktor der Ladung
	public Raumschiff () {}
	
	
	public Raumschiff (String schiffsname, String kapitansname, 
					   int energieversorgungProzent, int schieldeProzent, int lebenserhaltung, 
					   int huelleProzent, int photonentorpedoAnzahl, int reparaturandroidAnzahl,
					   ArrayList<String> broadcastKommunikator)
		{
			setSchiffsname(schiffsname);
			setKapitanname(kapitansname);
			setEenergieversorgungProzent(energieversorgungProzent);
			setSchutzschildeZustand(schieldeProzent);
			setLebenserhaltungssystemeZustand(lebenserhaltung);
			setHuelleZustand(huelleProzent);
			setPhotonentorpedosAnzahl(photonentorpedoAnzahl);
			setReparaturAndroidenAnzahl(reparaturandroidAnzahl);
			setBroadcastKommunikator(broadcastKommunikator);
		}
	
	//"getters and setters" für Attributen
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	public String getSchiffname() { return this.schiffsname; }
	
	public void setKapitanname(String kapitansname) {
		this.kapitansname = kapitansname;
	}
	public String getKapitaenname() { return this.kapitansname; }
	
	public void setEenergieversorgungProzent(int energieversorgungProzent) {
		if (energieversorgungProzent > 0)
			this.energieversorgungProzent = energieversorgungProzent;
		else
			this.energieversorgungProzent = 0;
	}
	public int getEnergieversorgungProzent() { return this.energieversorgungProzent; }
	
	public void setSchutzschildeZustand(int schutzschilde_zustand) {
		if (schutzschilde_zustand > 0)
			this.schieldeProzent = schutzschilde_zustand;
		else
			this.schieldeProzent = 0;
	}
	public int getSchutzschildeZustand() { return this.schieldeProzent; }
	
	public void setLebenserhaltungssystemeZustand(int lebenserhaltungssysteme_zustand) {
		if (lebenserhaltungssysteme_zustand > 0)
			this.lebenserhaltung = lebenserhaltungssysteme_zustand;
		else
			this.lebenserhaltung = 0;
	}
	public int getLebenserhaltungssystemeZustand() { return this.lebenserhaltung; }
	
	public void setHuelleZustand(int huelle_zustand) {
		if (huelle_zustand > 0)
			this.huelleProzent = huelle_zustand;
		else
			this.huelleProzent = 0;
	}
	public int getHuelleZustand() { return this.huelleProzent; }
	
	public void setPhotonentorpedosAnzahl(int photonentorpedos_anzahl) {
		if (photonentorpedos_anzahl > 0)
			this.photonentorpedoAnzahl = photonentorpedos_anzahl;
		else
			this.photonentorpedoAnzahl = 0;
	}
	public int getPhotonentorpedosAnzahl() { return this.photonentorpedoAnzahl; }
	
	public void setReparaturAndroidenAnzahl(int reparatur_androiden_anzahl) {
		if (reparatur_androiden_anzahl > 0)
			this.reparaturandroidAnzahl = reparatur_androiden_anzahl;
		else
			this.reparaturandroidAnzahl = 0;
	}
	public int getReparaturAndroidenAnzahl() { return this.reparaturandroidAnzahl; }
	
	public void setBroadcastKommunikator(ArrayList<String> broadcast_kommunikator) {
		this.broadcastKommunikator = broadcast_kommunikator;
	}
	public ArrayList<String> getBroadcastKommunikator() { return this.broadcastKommunikator; }
	
	//die Methode um die Fracht hinzufuegen 
	public void addLadung (Ladung neue_ladung) {
		neue_ladung.setRaumschiffname(getSchiffname());
		this.ladungsverzeichnis.add(neue_ladung);
	}
	
	//Die Methode im Console des Zustands auszugeben
	public void raumschiffZustandZeigen() {
		System.out.println("Schiffname: " + getSchiffname());
		System.out.println("Kapitänname: " + getKapitaenname());
		System.out.println("Energieversorgung: " + getEnergieversorgungProzent());
		System.out.println("Schutzschilde: " + getSchutzschildeZustand());
		System.out.println("Lebenserhaltungssysteme: " + getLebenserhaltungssystemeZustand());
		System.out.println("Hülle: " + getHuelleZustand());
		System.out.println("Photonentorpedos Anzahl: " + getPhotonentorpedosAnzahl());
		System.out.println("Reparaturandroiden Anzahl: " + getReparaturAndroidenAnzahl());	
	}
	
	//Die Methode die Fracht anzuzeigen
	public void ladungsverzeichnisZeigen() {
		System.out.println("Ladungsverzeichnis: ");
		for (Ladung l:this.ladungsverzeichnis) {
			System.out.println(l.getType() + ": " + l.getAnzahl());
		}
	}
	
	//Die Methode Thorpeden abzuschiessen
	public void photonentorpedoAbschießen(Raumschiff ziel) {
		int amount = getPhotonentorpedosAnzahl();
		if (amount > 0) {
			nachrichtSenden("Photonentorpedo abgeschossen");
			setPhotonentorpedosAnzahl(amount - 1);
			treffer(ziel);
		}
		else {
			nachrichtSenden("-=*Click*=-");
		}
		
	}
	
	//Die Methode Kanonen abzuschiessen
	public void phaserkanoneAbschießen(Raumschiff ziel) {
		int amount = getEnergieversorgungProzent();
		if (amount >= 50) {
			nachrichtSenden("Phaserkanone abgeschossen");
			setEenergieversorgungProzent(amount - 50);
			treffer(ziel);
		}
		else
			nachrichtSenden("-=*Click*=-");
	}
	
	//Die Methode der Treffungen zu merken
	public void treffer(Raumschiff ziel) {
		System.out.println(ziel.getSchiffname() + " wurde getroffen!");
		int schield = ziel.getSchutzschildeZustand();
		if (schield <= 0) {
			int huelle = ziel.getHuelleZustand();
			int energieversorgung = ziel.getEnergieversorgungProzent();
			if (huelle <= 0) {
				ziel.setLebenserhaltungssystemeZustand(0);
				ziel.nachrichtSenden("Die Lebenserhaltungssysteme sind vernichtet worden.");
			}
			
			else {
			ziel.setHuelleZustand(huelle - 50);
			ziel.setEenergieversorgungProzent(energieversorgung - 50);
				if (huelle <= 0) {
					ziel.setLebenserhaltungssystemeZustand(0);
					ziel.nachrichtSenden("Die Lebenserhaltungssysteme sind vernichtet worden.");
				}
			}
		}
		
		else
			ziel.setSchutzschildeZustand(schield - 50);
	}
	
	public void nachrichtSenden(String message) {
		this.broadcastKommunikator.add(message);
		System.out.println("Nachricht an Alle wurde gesendet: " + message);
	}
		
	public void logbuchZeigen() {
		System.out.println("Logbuch Einträge: ");
		for (String message:this.broadcastKommunikator) {
			System.out.println(message);
		}
	}
}
