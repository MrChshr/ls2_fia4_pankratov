package main;

public class Ladung {
	//Attributen der Ladung
	private String bezeichnung;
	private int menge;
	private String raumschiffsname;
	
	//Konstruktor der Ladung
	public Ladung () {}
	
	public Ladung (String bezeichnung, int menge) {
		setType(bezeichnung);
		setAnzahl(menge);
		setRaumschiffname("");
	}
	
	//"getters and setters" der Ladung
	public void setType (String type) {
		this.bezeichnung = type;
	}
	public String getType () { return this.bezeichnung; }
	
	public void setAnzahl (int anzahl) {
		if (anzahl > 0)
			this.menge = anzahl;
		else
			this.menge = 0;
	}
	public int getAnzahl () { return this.menge; }
	
	public void setRaumschiffname(String raumschiffname) {
		this.raumschiffsname = raumschiffname;
	}
	public String getRaumschiffname() { return this.raumschiffsname; }
	
}
