import java.awt.Point;

public class Rechteck {
	private double laenge;
	private double breite;
	private Point anfang;
    
	public Rechteck(double laenge, double breite, Point anfang)
	{
	  setStartpunkt(anfang);
	  setHigh(laenge);
	  setWide(breite);
	}
	
	public void setHigh(double high) {
		if(high > 0)
			this.laenge = high;
		else
			this.laenge = 0;
	}
	
	public double getHigh() {
		return this.laenge;
	}
	
	public void setWide(double wide) {
		if(wide > 0)
			this.breite = wide;
		else
			this.breite = 0;
	}
	
	public double getWide() {
		return this.breite;
	}
	
	public double getDurchmesser() {
		return Math.sqrt(Math.pow(this.laenge, 2) + Math.pow(this.breite, 2));
	}
	
	public double getFlaeche() {
		return this.laenge * this.breite;
	}
	
	public double getUmfang() {
		return 2 * this.laenge + 2 * this.breite;
	}

	public void setStartpunkt(Point startpunkt) {
		this.anfang = startpunkt; 
	}
	
	public Point getStartpunkt() {
		return this.anfang;
	}
}
